/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.webresource.geo.impl;

import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.stax.service.StAXFactoryService;
import net.coalevo.system.service.ExecutionService;
import com.arimdi.geo.service.GeoService;
import com.arimdi.encuentro.service.NetworkServices;
import org.osgi.framework.*;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private BundleContext m_BundleContext;

  private MessageResourceService m_MessageResourceService;
  private ExecutionService m_ExecutionService;
  private GeoService m_GeoService;
  private StatisticsService m_StatisticsService;
  private StAXFactoryService m_StAXFactoryService;
  private NetworkServices m_NetworkServices;

  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_ExecutionServiceLatch;
  private CountDownLatch m_GeoServiceLatch;
  private CountDownLatch m_StatisticsServiceLatch;
  private CountDownLatch m_StAXFactoryServiceLatch;
  private CountDownLatch m_NetworkServicesLatch;

  private ConfigurationMediator m_ConfigurationMediator;

  public ServiceMediator() {

  }//constructor

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public GeoService getGeoService(long wait) {
    try {
      if (wait < 0) {
        m_GeoServiceLatch.await();
      } else if (wait > 0) {
        m_GeoServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_GeoService;
  }//getGeoService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_ExecutionService;
  }//getExecutionService

  public StatisticsService getStatisticsService(long wait) {
    try {
      if (wait < 0) {
        m_StatisticsServiceLatch.await();
      } else if (wait > 0) {
        m_StatisticsServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_StatisticsService;
  }//getStatisticsService

  public StAXFactoryService getStAXFactoryService(long wait) {
    try {
      if (wait < 0) {
        m_StAXFactoryServiceLatch.await();
      } else if (wait > 0) {
        m_StAXFactoryServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_StAXFactoryService;
  }//getStAXFactoryService

  public NetworkServices getNetworkServices(long wait) {
    try {
      if (wait < 0) {
        m_NetworkServicesLatch.await();
      } else if (wait > 0) {
        m_NetworkServicesLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_NetworkServices;
  }//getNetworkServices

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigurationMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigurationMediator = configMediator;
  }//setConfigMediator

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_MessageResourceServiceLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();
    m_GeoServiceLatch = createWaitLatch();
    m_StatisticsServiceLatch = createWaitLatch();
    m_StAXFactoryServiceLatch = createWaitLatch();
    m_NetworkServicesLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(|(objectclass=" + MessageResourceService.class.getName() + ")" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + GeoService.class.getName() + "))" +
            "(objectclass=" + StAXFactoryService.class.getName() + "))" +
            "(objectclass=" + StatisticsService.class.getName() + "))" +
            "(objectclass=" + NetworkServices.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MessageResourceService = null;
    m_GeoService = null;
    m_ExecutionService = null;
    m_ConfigurationMediator = null;
    m_StatisticsService = null;
    m_StAXFactoryService = null;
    m_NetworkServices = null;

    //Latches
    m_MessageResourceServiceLatch = null;
    m_ExecutionServiceLatch = null;
    m_GeoServiceLatch = null;
    m_StatisticsServiceLatch = null;
    m_StAXFactoryServiceLatch = null;
    m_NetworkServicesLatch = null;

    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      //System.err.println(ev.toString());
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
          } else if (o instanceof GeoService) {
            m_GeoService = (GeoService) o;
            /*
            //obtain props
            Object o2 = sr.getProperty("arimdi.geo.id");
            if(o2 == null) {
              o2 = "";
            }
            m_PhotoStoreID = o2.toString();
            */
            //release latch
            m_GeoServiceLatch.countDown();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = (StatisticsService) o;
            m_StatisticsServiceLatch.countDown();
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = (StAXFactoryService) o;
            m_StAXFactoryServiceLatch.countDown();
          } else if (o instanceof NetworkServices) {
            m_NetworkServices = (NetworkServices) o;
            m_NetworkServicesLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
          } else if (o instanceof GeoService) {
            m_GeoService = null;
            m_GeoServiceLatch = createWaitLatch();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = null;
            m_StatisticsServiceLatch = createWaitLatch();
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = null;
            m_StAXFactoryServiceLatch = createWaitLatch();
          } else if (o instanceof NetworkServices) {
            m_NetworkServices = null;
            m_NetworkServicesLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
