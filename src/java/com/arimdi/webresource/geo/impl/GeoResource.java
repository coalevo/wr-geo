/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.webresource.geo.impl;

import com.arimdi.geo.model.GeoLocation;
import com.arimdi.geo.model.GeoLocationName;
import com.arimdi.geo.model.NoSuchLocationException;
import com.arimdi.geo.service.GeoService;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.webresource.model.BaseWebResource;
import net.coalevo.webresource.model.WebResourceException;
import net.coalevo.webresource.model.WebResourceRequest;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.HashSet;
import java.util.Set;


/**
 * This class implements the webresource for the Photo store.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class GeoResource extends BaseWebResource {

  private static Marker c_LogMarker = MarkerFactory.getMarker(GeoResource.class.getName());
  private ServiceMediator m_Services = Activator.getServices();

  public GeoResource() {
    super("geo");
  }//constructor

  public void handle(WebResourceRequest resreq) throws WebResourceException {
    //1. prepare resources
    GeoService gs = m_Services.getGeoService(ServiceMediator.NO_WAIT);
    ServiceAgent sag = resreq.getServiceAgent();

    //2. assert run
    if (gs == null || sag == null) {
      Activator.log().error("gs or sag == null!");
      resreq.failInternalError();
      return;
    }

    //3. assert subresource
    if (!resreq.hasSubResources()) {
      resreq.failBadRequest();
      return;
    }

    String[] subres = resreq.getSubResources();
    if (subres.length == 1) {
      if ("rebuilddb".equals(subres[0])) {
        handleRebuild(sag, resreq, gs);
        return;
      } else if ("continents".equals(subres[0])) {
        handleContinents(resreq, gs);
        return;
      } else if ("countries".equals(subres[0])) {
        handleCountries(resreq, gs);
        return;
      } else if ("regions".equals(subres[0])) {
        if (!resreq.hasParameter(PARAM_PREFIX)) {
          resreq.failBadRequest();
          return;
        }
        handleRegions(resreq, gs);
        return;
      } else if ("cities".equals(subres[0])) {
        if (!resreq.hasParameter(PARAM_PREFIX)) {
          resreq.failBadRequest();
          return;
        }
        handleCities(resreq, gs);
        return;
      } else {
        resreq.failBadRequest();
        return;
      }

    } else if (subres.length == 2) {
      if (CITY.equals(subres[0])) {
        try {
          Long gid = Long.valueOf(subres[1]);
          handleCityLocation(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      } else if (REGION.equals(subres[0])) {
        try {
          Long gid = Long.valueOf(subres[1]);
          handleRegionLocation(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      } else if ("name".equals(subres[0])) {
        try {
          Long gid = Long.valueOf(subres[1]);
          handleName(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      } else if ("id".equals(subres[0])) {
         handleIdentifier(resreq, gs, subres[1]);
         return;
      } else if ("names".equals(subres[0])) {
        try {
          Long gid = Long.valueOf(subres[1]);
          handleNames(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      }
    } else if (subres.length == 3) {
      if (REGION.equals(subres[0]) && "forcity".equals(subres[1])) {
        try {
          Long gid = Long.valueOf(subres[2]);
          handleRegionForCity(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      } else if (COUNTRY.equals(subres[0]) && "forregion".equals(subres[1])) {
        try {
          Long gid = Long.valueOf(subres[2]);
          handleCountryForRegion(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      } else if (CONTINENT.equals(subres[0]) && "forcountry".equals(subres[1])) {
        try {
          Long gid = Long.valueOf(subres[2]);
          handleContinentForCountry(resreq, gs, gid);
          return;
        } catch (NumberFormatException ex) {
          resreq.failBadRequest();
          return;
        }
      }
    }
    resreq.failBadRequest();
  }//handle

  private void handleRebuild(Agent a, WebResourceRequest resreq, GeoService gs) {
    try {
      gs.rebuildDatabase(a);
    } catch (Exception ex) {
      Activator.log().error("handleRebuild()", ex);
      resreq.failInternalError();
    }
    resreq.success();
  }//handleRebuild

   private void handleName(WebResourceRequest resreq, GeoService gs, Long id) {
    try {
      GeoLocationName gln;
      try {
        if (resreq.hasParameter(PARAM_LANG)) {
          gln = gs.getName(id, resreq.getParameter(PARAM_LANG));
        } else {
          gln = gs.getName(id);
        }
      } catch (NoSuchLocationException nslex) {
        resreq.failNoContent();
        return;
      }
      Set<GeoLocationName> s = new HashSet<GeoLocationName>();
      s.add(gln);
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), "any", s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), "any", s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleName()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleName

   private void handleNames(WebResourceRequest resreq, GeoService gs, long gid) {
    try {
      Set<GeoLocationName> s = gs.listNames(gid);
      if (s.isEmpty()) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), "any", s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), "any", s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleNames()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleNames
  
  private void handleIdentifier(WebResourceRequest resreq, GeoService gs, String name) {
   try {
     try {
       resreq.quickResponse(Long.toString(gs.getIdentifierForName(name)));
     } catch (NoSuchLocationException nslex) {
       resreq.failNoContent();
       return;
     }
   } catch (Exception ex) {
     Activator.log().error("handleIdentifier()", ex);
     resreq.failBadRequest();
     return;
   }
 }//handleIdentifier

  private void handleContinents(WebResourceRequest resreq, GeoService gs) {
    String prefix = resreq.getParameter(PARAM_PREFIX);
    try {
      Set<GeoLocationName> s;
      if (resreq.hasParameter(PARAM_LANG)) {
        if (prefix == null || prefix.length() == 0) {
          s = gs.listContinents(resreq.getParameter(PARAM_LANG));
        } else {
          s = gs.listContinentsByName(prefix, resreq.getParameter(PARAM_LANG));
        }
      } else {
        if (prefix == null || prefix.length() == 0) {
          s = gs.listContinents();
        } else {
          s = gs.listContinentsByName(prefix);
        }
      }
      if (s.isEmpty()) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), CONTINENT, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), CONTINENT, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleContinents()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleContinents

    private void handleContinentForCountry(WebResourceRequest resreq, GeoService gs, Long id) {
    try {
      GeoLocationName gln;
      try {
        if (resreq.hasParameter(PARAM_LANG)) {
          gln = gs.getContinentForCountry(id, resreq.getParameter(PARAM_LANG));
        } else {
          gln = gs.getContinentForCountry(id);
        }
      } catch (NoSuchLocationException nslex) {
        resreq.failNoContent();
        return;
      }
      Set<GeoLocationName> s = new HashSet<GeoLocationName>();
      s.add(gln);
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), CONTINENT, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), CONTINENT, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleContinentForCountry()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleContinentForCountry

  private void handleCountries(WebResourceRequest resreq, GeoService gs) {
    String prefix = resreq.getParameter(PARAM_PREFIX);
    try {
      Set<GeoLocationName> s;
      if (resreq.hasParameter(PARAM_LANG)) {
        if (prefix == null || prefix.length() == 0) {
          s = gs.listCountries(resreq.getParameter(PARAM_LANG));
        } else {
          s = gs.listCountriesByName(prefix, resreq.getParameter(PARAM_LANG));
        }
      } else {
        if (prefix == null || prefix.length() == 0) {
          s = gs.listCountries();
        } else {
          s = gs.listCountriesByName(prefix);
        }
      }
      if (s.isEmpty()) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), COUNTRY, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), COUNTRY, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleCountries()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleCountries

  private void handleCountryForRegion(WebResourceRequest resreq, GeoService gs, Long id) {
    try {
      GeoLocationName gln;
      try {
        if (resreq.hasParameter(PARAM_LANG)) {
          gln = gs.getCountryForRegion(id, resreq.getParameter(PARAM_LANG));
        } else {
          gln = gs.getCountryForRegion(id);
        }
      } catch (NoSuchLocationException nslex) {
        resreq.failNoContent();
        return;
      }
      Set<GeoLocationName> s = new HashSet<GeoLocationName>();
      s.add(gln);
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), COUNTRY, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), COUNTRY, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleCountryForRegion()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleCountryForRegion

  private void handleRegions(WebResourceRequest resreq, GeoService gs) {
    String prefix = resreq.getParameter(PARAM_PREFIX);
    if (prefix.length() < 1) {
      resreq.failBadRequest();
      return;
    }
    try {
      Set<GeoLocationName> s;
      if (resreq.hasParameter(PARAM_LANG)) {
        s = gs.listRegionsByName(prefix, resreq.getParameter(PARAM_LANG));
      } else {
        s = gs.listRegionsByName(prefix);
      }
      if (s.isEmpty()) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), REGION, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), REGION, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleRegions()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleRegions

  private void handleRegionForCity(WebResourceRequest resreq, GeoService gs, Long id) {
    try {
      GeoLocationName gln;
      try {
        if (resreq.hasParameter(PARAM_LANG)) {
          gln = gs.getRegionForCity(id, resreq.getParameter(PARAM_LANG));
        } else {
          gln = gs.getRegionForCity(id);
        }
      } catch (NoSuchLocationException nslex) {
        resreq.failNoContent();
        return;
      }
      Set<GeoLocationName> s = new HashSet<GeoLocationName>();
      s.add(gln);
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), REGION, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), REGION, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleRegionForCity()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleRegionForCity

  private void handleRegionLocation(WebResourceRequest resreq, GeoService gs, Long gid) {
    try {
      GeoLocation gl;
      try {
        gl = gs.getRegionLocation(gid);
      } catch (NoSuchLocationException ex) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), REGION, gl);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), REGION, gl);
      }

    } catch (Exception ex) {
      Activator.log().error("handleRegionLocation()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleRegionLocation

  private void handleCities(WebResourceRequest resreq, GeoService gs) {
    String prefix = resreq.getParameter(PARAM_PREFIX);
    if (prefix.length() < 3) {
      resreq.failBadRequest();
      return;
    }
    try {
      Set<GeoLocationName> s;
      if (resreq.hasParameter(PARAM_LANG)) {
        s = gs.listCitiesByName(prefix, resreq.getParameter(PARAM_LANG));
      } else {
        s = gs.listCitiesByName(prefix);
      }
      if (s.isEmpty()) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), CITY, s);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), CITY, s);
      }

    } catch (Exception ex) {
      Activator.log().error("handleCities()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleCities

  private void handleCityLocation(WebResourceRequest resreq, GeoService gs, Long gid) {
    try {
      GeoLocation gl;
      try {
        gl = gs.getCityLocation(gid);
      } catch (NoSuchLocationException ex) {
        resreq.failNoContent();
        return;
      }
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        //JSON
        resreq.setJSONResponseType();
        Marshaller.toJSON(resreq.getOutputWriter(), CITY, gl);
      } else {
        //XML
        resreq.setXMLResponseType();
        Marshaller.toXML(resreq.getOutputWriter(), CITY, gl);
      }

    } catch (Exception ex) {
      Activator.log().error("handleCityLocation()", ex);
      resreq.failBadRequest();
      return;
    }
  }//handleCityLocation

  private static final String PARAM_FORMAT = "format";
  private static final String PARAM_LANG = "lang";
  private static final String PARAM_PREFIX = "prefix";
  private static final String CITY = "city";
  private static final String REGION = "region";
  private static final String COUNTRY = "country";
  private static final String CONTINENT = "continent";

}//class GeoResource
