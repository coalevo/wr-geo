/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.webresource.geo.impl;

import com.arimdi.geo.model.GeoLocation;
import com.arimdi.geo.model.GeoLocationName;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

/**
 * This class...
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class Marshaller {

  private static JsonFactory m_JsonFactory = new JsonFactory();

  static void toJSON(PrintWriter pw, String type, Set<GeoLocationName> s)
      throws IOException {

    JsonGenerator w = m_JsonFactory.createJsonGenerator(pw);
    w.writeStartObject();
    writeStringField(w, ATTR_TYPE, type);
    writeIntField(w, ATTR_NUM_RESULTS, s.size());

    w.writeFieldName(ELEM_RESULTS);
    w.writeStartArray();
    for (GeoLocationName gln : s) {
      writeGeoLocationName(w, gln);
    }
    w.writeEndArray();
    w.writeEndObject(); //continents
    w.flush();
  }//toJSON

  static void toJSON(PrintWriter pw, String type, GeoLocation gl)
      throws IOException {

    JsonGenerator w = m_JsonFactory.createJsonGenerator(pw);
    w.writeStartObject();
    writeStringField(w, ATTR_TYPE, type);
    w.writeFieldName(ELEM_RESULTS);
    writeGeoLocation(w, gl);
    w.flush();
  }//toJSON

  static void toXML(PrintWriter pw, String type, Set<GeoLocationName> s)
      throws Exception {

    XMLStreamWriter w = Activator.getServices().getStAXFactoryService(ServiceMediator.NO_WAIT)
        .getXMLOutputFactory().createXMLStreamWriter(pw);

    w.writeStartElement(ELEM_RESPONSE);
    w.writeAttribute(ATTR_TYPE, type);
    w.writeAttribute(ATTR_NUM_RESULTS, Integer.toString(s.size()));
    for (GeoLocationName gln : s) {
      writeGeoLocationName(w, gln);
    }
    w.writeEndElement();
    w.flush();
  }//toXML

  static void toXML(PrintWriter pw, String type, GeoLocation gl)
      throws Exception {

    XMLStreamWriter w = Activator.getServices().getStAXFactoryService(ServiceMediator.NO_WAIT)
        .getXMLOutputFactory().createXMLStreamWriter(pw);

    w.writeStartElement(ELEM_RESPONSE);
    w.writeAttribute(ATTR_TYPE, type);
    writeGeoLocation(w, gl);
    w.writeEndElement();
    w.flush();
  }//toXML

  /*
    static void toXML(PrintWriter pw, PhotoStoreInfo info) throws Exception {
      XMLStreamWriter w = Activator.getServices().getStAXFactoryService(ServiceMediator.NO_WAIT).getXMLOutputFactory().createXMLStreamWriter(pw);

      w.writeStartElement(ELEM_PHOTOSTOREINFO);
      w.writeAttribute(ATTR_ID, info.getIdentifier());

      // Size
      w.writeStartElement(ELEM_SIZE);
      w.writeCharacters(info.getSize().toString(10));
      w.writeEndElement();

      // Usage
      w.writeStartElement(ELEM_USAGE);
      w.writeAttribute(ATTR_TOTAL, info.getUsed().toString(10));
      w.writeAttribute(ATTR_ORIGINALS, info.getSizeOfStoredOriginals().toString(10));
      w.writeAttribute(ATTR_INFOS, info.getSizeOfStoredInfos().toString(10));

      // available
      w.writeStartElement(ELEM_AVAILABLE);
      w.writeAttribute(ATTR_TOTAL, info.getAvailable().toString(10));
      w.writeAttribute(ATTR_CAPACITY, Float.toString(info.getCapacity()));
      w.writeEndElement();

      w.writeEndElement(); //usage

      // Images
      w.writeStartElement(ELEM_PHOTOS);
      w.writeAttribute(ATTR_TOTAL,Long.toString(info.getNumberOfStoredImages()));
      w.writeAttribute(ATTR_ORIGINALS,Long.toString(info.getNumberOfStoredOriginals()));
      w.writeEndElement();

      w.writeEndElement();//imagestore-info
      w.flush();
    }//toXML

    static void toJSON(PrintWriter pw, PhotoStoreInfo info)
        throws IOException {

      JsonGenerator w = m_JsonFactory.createJsonGenerator(pw);
      w.writeStartObject(); //imagestore-info
      writeStringField(w, ATTR_ID, info.getIdentifier());
      writeStringField(w, ELEM_SIZE, info.getSize().toString(10));
      // Usage
      w.writeFieldName(ELEM_USAGE);
      w.writeStartObject();//START: usage
      writeStringField(w, ATTR_TOTAL, info.getUsed().toString(10));
      writeStringField(w, ATTR_ORIGINALS, info.getSizeOfStoredOriginals().toString(10));
      writeStringField(w, ATTR_INFOS, info.getSizeOfStoredInfos().toString(10));
      w.writeFieldName(ELEM_AVAILABLE);
      w.writeStartObject(); //START: available
      writeStringField(w, ATTR_TOTAL, info.getAvailable().toString(10));
      writeFloatField(w, ATTR_CAPACITY, info.getCapacity());
      w.writeEndObject();//END: available
      w.writeEndObject(); //END: usage
      // Images
      w.writeFieldName(ELEM_PHOTOS);
      w.writeStartObject();
      writeLongField(w, ATTR_TOTAL, info.getNumberOfStoredImages());
      writeLongField(w, ATTR_ORIGINALS, info.getNumberOfStoredOriginals());
      w.writeEndObject(); //images
      w.writeEndObject(); //imagestore-info
      w.flush();
    }//toJSON


    static void toXML(PrintWriter pw, PhotoInfo info) throws Exception {
      XMLStreamWriter w = Activator.getServices().getStAXFactoryService(ServiceMediator.NO_WAIT).getXMLOutputFactory().createXMLStreamWriter(pw);

      w.writeStartElement(ELEM_IMAGEINFO);
      w.writeAttribute(ATTR_ID, info.getIdentifier());
      w.writeAttribute(ATTR_ADDED, Long.toString(info.added()));
      w.writeAttribute(ATTR_FITS, Integer.toString(info.fits()));
      w.writeAttribute(ATTR_ORIGSTORED, Boolean.toString(info.isOriginalStored()));

      for (int i : info.availableFits()) {
        PhotoSize is = info.getSize(i);
        // Size
        w.writeStartElement(ELEM_SIZE);
        w.writeAttribute(ATTR_FIT, Integer.toString(is.getFit()));
        w.writeAttribute(ATTR_WIDTH, Integer.toString(is.getWidth()));
        w.writeAttribute(ATTR_HEIGHT, Integer.toString(is.getHeight()));
        w.writeAttribute(ATTR_BYTES, Long.toString(is.getSize()));
        w.writeEndElement();
      }

      w.writeEndElement();
      w.flush();
    }//toXML

    static void toJSON(PrintWriter pw, PhotoInfo info)
        throws IOException {

      JsonGenerator w = m_JsonFactory.createJsonGenerator(pw);
      w.writeStartObject(); //images-info
      writeStringField(w, ATTR_ID, info.getIdentifier());
      writeLongField(w, ATTR_ADDED, info.added());
      writeIntField(w, ATTR_FITS, info.fits());
      writeBooleanField(w, ATTR_ORIGSTORED, info.isOriginalStored());

      w.writeFieldName(ELEM_SIZES);
      w.writeStartArray();
      for (int i : info.availableFits()) {
        PhotoSize is = info.getSize(i);
        w.writeStartObject();
        writeIntField(w, ATTR_FIT, is.getFit());
        writeIntField(w, ATTR_WIDTH, is.getWidth());
        writeIntField(w, ATTR_HEIGHT, is.getHeight());
        writeLongField(w, ATTR_BYTES, is.getSize());
        w.writeEndObject();
      }
      w.writeEndArray();
      w.writeEndObject();
      w.flush();
    }//toJSON

  */

  ///* XML Utilities *///

  private static void writeStringElement(XMLStreamWriter w, String element, String content)
      throws Exception {
    w.writeStartElement(element);
    w.writeCharacters(content);
    w.writeEndElement();
  }//writeStringElement

  private static void writeGeoLocationName(XMLStreamWriter w, GeoLocationName gln)
      throws Exception {
    w.writeStartElement(ELEM_GEOLOCATIONNAME);
    w.writeAttribute(ATTR_ID, Long.toString(gln.getIdentifier()));
    w.writeAttribute(ATTR_LANG, gln.getLanguage());
    if (gln.hasShortcut()) {
      w.writeAttribute(ATTR_SHORTCUT, gln.getShortcut());
    }
    w.writeCharacters(gln.getName());
    w.writeEndElement();
  }//writeGeoLocationName

  private static void writeGeoLocation(XMLStreamWriter w, GeoLocation gl)
      throws Exception {
    w.writeStartElement(ELEM_GEOLOCATION);
    w.writeAttribute(ATTR_ID, Long.toString(gl.getIdentifier()));
    writeStringElement(w, ELEM_LAT, Double.toString(gl.getLatitude()));
    writeStringElement(w, ELEM_LONG, Double.toString(gl.getLongitude()));
    w.writeEndElement();
  }//writeGeoLocation

  ///* JSON Utilities *///

  private static void writeGeoLocationName(JsonGenerator writer, GeoLocationName gln)
      throws IOException {
    writer.writeStartObject();
    writeLongField(writer, ATTR_ID, gln.getIdentifier());
    writeStringField(writer, ELEM_NAME, gln.getName());
    writeStringField(writer, ATTR_LANG, gln.getLanguage());
    if (gln.hasShortcut()) {
      writeStringField(writer, ATTR_SHORTCUT, gln.getShortcut());
    }
    writer.writeEndObject();
  }//writeGeoLocationName

  private static void writeGeoLocation(JsonGenerator writer, GeoLocation gl)
      throws IOException {
    writer.writeStartObject();
    writeLongField(writer, ATTR_ID, gl.getIdentifier());
    writeStringField(writer, ELEM_LAT, Double.toString(gl.getLatitude()));
    writeStringField(writer, ELEM_LONG, Double.toString(gl.getLongitude()));
    writer.writeEndObject();
  }//writeGeoLocation

  private static void writeStringField(JsonGenerator writer, String element, String content)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeString(content);
  }//writeStringField

  private static void writeBooleanField(JsonGenerator writer, String element, boolean b)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeBoolean(b);
  }//writeBooleanField

  private static void writeLongField(JsonGenerator writer, String element, long l)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeNumber(l);
  }//writeLongField

  private static void writeFloatField(JsonGenerator writer, String element, float f)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeNumber(f);
  }//writeFloatField

  private static void writeIntField(JsonGenerator writer, String element, int i)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeNumber(i);
  }//writeIntField


  private static final String ELEM_RESPONSE = "response";
  private static final String ELEM_NAME = "name";
  private static final String ELEM_RESULTS = "results";
  private static final String ELEM_GEOLOCATIONNAME = "geoname";
  private static final String ELEM_GEOLOCATION = "geoloc";
  private static final String ELEM_LAT = "lat";
  private static final String ELEM_LONG = "long";

  private static final String ATTR_TYPE = "type";
  private static final String ATTR_NUM_RESULTS = "count";
  private static final String ATTR_ID = "id";
  private static final String ATTR_LANG = "lang";
  private static final String ATTR_SHORTCUT = "shortcut";


  /*
  private static final String ELEM_PHOTOSTOREINFO = "continents";
  private static final String ELEM_SIZE = "size";
  private static final String ELEM_SIZES = "sizes";
  private static final String ELEM_USAGE = "usage";
  private static final String ELEM_AVAILABLE = "available";
  private static final String ELEM_PHOTOS = "photos";
  private static final String ATTR_ID = "id";
  private static final String ATTR_TOTAL = "total";
  private static final String ATTR_ORIGINALS = "originals";
  private static final String ATTR_INFOS = "infos";
  private static final String ATTR_CAPACITY = "capacity";

  private static final String ELEM_IMAGEINFO = "photo-info";
  private static final String ATTR_ADDED = "added";
  private static final String ATTR_FITS = "fits";
  private static final String ATTR_ORIGSTORED = "origStored";
  private static final String ATTR_FIT = "fit";
  private static final String ATTR_WIDTH = "width";
  private static final String ATTR_HEIGHT = "height";
  private static final String ATTR_BYTES = "bytes";

*/

}//class Marshaller
