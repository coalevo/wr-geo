/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.webresource.geo.impl;

import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.foundation.util.DummyMessages;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.webresource.model.WebResource;
import com.arimdi.encuentro.model.Service;
import com.arimdi.encuentro.model.ServiceConflict;
import com.arimdi.encuentro.model.ServiceProperty;
import com.arimdi.encuentro.model.TransportType;
import com.arimdi.encuentro.service.NetworkServices;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.net.InetAddress;

/**
 * This class implements the bundle activator.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator implements BundleActivator {

  private static LogProxy c_Log;
  private static Marker c_LogMarker;

  private static Messages c_BundleMessages = new DummyMessages();
  private static BundleConfiguration c_BundleConfiguration;
  private static ServiceMediator c_Services;
  private static GeoResource c_Webresource;
  private static Service c_ServiceDescriptor;

  public void start(final BundleContext bundleContext) throws Exception {
    Thread t = new Thread(
        new Runnable() {
          public void run() {
            try {

              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //3. BundleConfiguration
              //c_BundleConfiguration = new BundleConfiguration(Configuration.class.getName());
              //c_BundleConfiguration.activate(bundleContext);
              //c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //4. GeoResource
              c_Webresource = new GeoResource();
              String[] classes = {WebResource.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_Webresource,
                  null
              );

              //5. Register Network Service
              NetworkServices ns = c_Services.getNetworkServices(ServiceMediator.WAIT_UNLIMITED);
              c_ServiceDescriptor = ns.createService();
              c_ServiceDescriptor.setAddress(InetAddress.getByName(c_Webresource.getAddress()));
              c_ServiceDescriptor.setPort(Integer.parseInt(c_Webresource.getPort()));
              c_ServiceDescriptor.setTransportType(TransportType.STREAM);
              c_ServiceDescriptor.setType("http");
              c_ServiceDescriptor.setName("geo");

              //Service property: Endpoint
              ServiceProperty sp = c_ServiceDescriptor.createProperty("ep", c_Webresource.getEndpoint());
              c_ServiceDescriptor.addProperty(sp);

              //Service property: ARIMDI node type
              sp = c_ServiceDescriptor.createProperty("ant","service");
              c_ServiceDescriptor.addProperty(sp);

              //Service property: ARIMDI store type
              sp = c_ServiceDescriptor.createProperty("ast","geo");
              c_ServiceDescriptor.addProperty(sp);

              c_Log.info("Activator::Registering = " + c_ServiceDescriptor.toString());

              try {
                ns.register(c_ServiceDescriptor);
              } catch (ServiceConflict sc) {
                c_Log.error("start()::run()", sc);
              }
            } catch (Exception ex) {
              log().error(c_LogMarker, "start(BundleContext)", ex);
            }
          }//run
        }//Runnable
    );//Thread
    t.setContextClassLoader(getClass().getClassLoader());
    t.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    if (c_ServiceDescriptor != null) {
      try {
        NetworkServices ns = c_Services.getNetworkServices(ServiceMediator.WAIT_UNLIMITED);
        ns.unregister(c_ServiceDescriptor);
      } catch (Exception ex) {
        c_Log.error("stop()", ex);
      }
    }
    if (c_BundleConfiguration != null) {
      c_BundleConfiguration.deactivate();
      c_BundleConfiguration = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
